package za.co.wethinkcode.model;

import za.co.wethinkcode.Main;
import za.co.wethinkcode.model.character.enemies.Enemy;
import za.co.wethinkcode.model.character.heros.Hero;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import lombok.Getter;
import lombok.Setter;

import javax.swing.text.BadLocationException;

@Getter
@Setter
public class Map
{
    /*
        • Level 1: (1 - 1) * 5 + 10 - ( 1 % 2 ) = 9  (^2 : 81 / 2  = 40.5)
        • Level 2: (2 - 1) * 5 + 10 - ( 2 % 2 ) = 15 (^2 : 225 / 2 = 112.5)
        • Level 3: (3 - 1) * 5 + 10 - ( 3 % 2 ) = 19 (^2 : 361 / 2 = 180.5)
        • Level 4: (4 - 1) * 5 + 10 - ( 4 % 2 ) = 25 (^2 : 625 / 2 = 312.5)
        • Level 5: (5 - 1) * 5 + 10 - ( 5 % 2 ) = 29 (^2 : 841 / 2 = 420.5)
    */

    public int mapSize;
    // starting new game or level
    public Map()
    {
        GameModel gameModel = Main.gameModel;
        gameModel.enemies   = new ArrayList<Enemy>();

        this.mapSize        = this.workoutMapSize(gameModel.player.getLevel());
    }

    public int workoutMapSize(int level)
    {
        return (level - 1) * 5 + 10 - ( level % 2);
    }

    public int getMapMiddle(int level)
    {
        float middlePosition    = (level - 1) * 5 + 10 - ( level % 2);
        middlePosition          = middlePosition / 2;
        middlePosition          = Math.round(middlePosition);

        return (int) middlePosition;
    }

    public int detectCollision() throws SQLException, IOException, BadLocationException, InterruptedException {
        // 0 for no encounter
        // 1 for enemy encounter
        // 2 for wall encounter
        // 3 win encounter

        GameModel gameModel = Main.gameModel;
        Hero player         = gameModel.player;

        int playerX         = player.getPosition().getCoordinateX();
        int playerY         = player.getPosition().getCoordinateY();

        int level           = player.getLevel();
        int mapSize         = (level - 1) * 5 + 10 - ( level % 2);

        for (int i = 0; i < gameModel.enemies.size(); i++)
        {
            Position enemyPosition = gameModel.enemies.get(i).getPosition();

            if ((enemyPosition.getCoordinateX() == playerX) && (enemyPosition.getCoordinateY() == playerY))
                return 1;
            else if  (playerX > mapSize || playerY > mapSize || playerX <= 0 || playerY <= 0 )
            {
                player.setPosition(player.getPreviousPosition());
                gameModel.endGame("win");
                return 2;
            }
            // else if (level == 5 && (playerX > mapSize || playerY > mapSize || playerX <= 0 || playerY <= 0))
            //     return 3;
        }

        player.gainLife(5);
        gameModel.saveProgress();

        return 0;
    }

    public void expandMap() throws SQLException, IOException, BadLocationException
    {
        GameModel gameModel = Main.gameModel;
        Hero player         = gameModel.player;

        int middle          = this.getMapMiddle(player.getLevel());

        player.setPosition(new Position(middle, middle));

        gameModel.gameMap = new Map();
        gameModel.setEnemyPositions(gameModel.gameMap.mapSize);

        gameModel.saveProgress();

        return;
    }

}

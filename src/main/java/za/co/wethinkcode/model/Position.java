package za.co.wethinkcode.model;

import lombok.Getter;

@Getter
public class Position
{
    private int coordinateX;
    private int coordinateY;

    public Position(int coordinateX, int coordinateY)
    {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }
}

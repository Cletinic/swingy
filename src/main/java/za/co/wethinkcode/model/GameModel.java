package za.co.wethinkcode.model;

import za.co.wethinkcode.Main;
import za.co.wethinkcode.controller.GameController;
import za.co.wethinkcode.model.character.heros.Hero;
import za.co.wethinkcode.model.character.enemies.Enemy;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import lombok.Getter;
import lombok.Setter;

import javax.swing.text.BadLocationException;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class GameModel
{
    public boolean gameRunning;
    public Hero player;

    public Map gameMap;
    public int[] gameLevel;
    public Logger gameLogger;

    public List<Enemy> enemies;

    public GameModel() throws SQLException, ClassNotFoundException
    {
        // initialise these values to null
        this.player     = null;
        this.gameMap    = null;

        this.gameLevel  = gameLevels();

        this.gameLogger = new Logger();
        this.gameLogger.createDataBase();
    }
    // STATES
    public void saveProgress() throws SQLException, IOException, BadLocationException {

        this.gameLogger.saveToDataBase(this.player);
        return;
    }

    public void endGame(String condition) throws SQLException, IOException, BadLocationException, InterruptedException
    {
        GameController gameController = Main.gameController;

        if (condition.toLowerCase().contains("win") || condition.toLowerCase().contains("dead"))
        {
            this.gameLogger.deleteFromDataBase(this.player.getName());

            if (condition.toLowerCase().equals("win")) gameController.controllerGameState("win");
            else gameController.controllerGameState("dead");

            System.exit(0);
            return;
        }
        else if (condition.toLowerCase().contains("close"))
        {
            if (condition.toLowerCase().equals("close"))
                this.saveProgress();

            gameController.controllerGameState("close");

            System.exit(0);
            return;
        }

        this.saveProgress();
        return;
    }

    // LEVELS
    // Should probably be in a level class
    private int[] gameLevels()
    {
        /*
            • level[0] = Level 1:  0 - 1000 XP      = (0 + 1) × 1000 + ((0 + 1) - 1)^2 × 450 = 1000
            • level[1] = Level 2:  1001 - 2450 XP   = (1 + 1) × 1000 + ((1 + 1) - 1)^2 × 450 = 2450
            • level[2] = Level 3:  2451 - 4800 XP   = (2 + 1) × 1000 + ((2 + 1) - 1)^2 × 450 = 4800
            • level[3] = Level 4:  4801 - 8050 XP   = (3 + 1) × 1000 + ((3 + 1) - 1)^2 × 450 = 8050
            • level[4] = Level 5:  8051 - 12200 XP  = (4 + 1) × 1000 + ((4 + 1) - 1)^2 × 450 = 12200
        */

        int[] level = new int[5];

        for (int i = 0; i < level.length; i++)
        {
            // max XP for each level
            level[i] = (i + 1) * 1000 + ((int)(Math.pow(((i + 1) - 1),2))) * 450;
        }

        return level;
    }

    public int workoutLevel(int xp)
    {
        int level = 1;

        for (int i = 0; i < this.gameLevel.length ; i++)
        {
            if (xp >= this.gameLevel[this.gameLevel.length - 1])
                return 5;
            else
            {
                if ((xp > this.gameLevel[i]) && (this.gameLevel[i + 1] !=  this.gameLevel[ this.gameLevel.length - 1]) && (xp < this.gameLevel[i + 1]))
                {
                    if (level != (i + 1))
                        level = (i + 1);
                }
            }
        }

        return level;
    }


    // ENEMY MANAGEMENT
    // should be in own class

    // public void setEnemyPositions(int mapSize)
    // {
    //     if (this.player.getPosition() == null)
    //     {
    //         int startPosition       = this.gameMap.getMapMiddle(this.player.getLevel()); //Map.getMapMiddle(this.level);
    //
    //         this.player.setPosition(new Position(startPosition, startPosition));
    //     }
    //
    //     int playerX                 = this.player.getPosition().getCoordinateX();
    //     int playerY                 = this.player.getPosition().getCoordinateY();
    //
    //     int numEnemies              = (int)(Math.round(Math.pow(mapSize, 2) / 2));
    //
    //     List<Position> positions    = new ArrayList<Position>();
    //
    //     int i = 0;
    //
    //     int coordX;
    //     int coordY;
    //     boolean contains;
    //
    //     System.out.println("enemy num: " + numEnemies);
    //
    //     while (i < (numEnemies))
    //     {
    //
    //         coordX = this.randomGen(1, mapSize);
    //         coordY = this.randomGen(1, mapSize);
    //
    //         contains = false;
    //
    //         if (coordX != playerX && coordY != playerY)
    //         {
    //             for (int j = 0; j < positions.size(); j++)
    //             {
    //                 Position enemPos = positions.get(j);
    //                 if (enemPos.getCoordinateX() == coordX && enemPos.getCoordinateY() == coordY)
    //                 {
    //                     contains = true;
    //                     break;
    //                 }
    //             }
    //
    //             if (!contains)
    //             {
    //                 positions.add(new Position(coordX, coordY));
    //                 i++;
    //             }
    //         }
    //     }
    //
    //     for (i = 0; i < positions.size(); i++)
    //     {
    //
    //         //System.out.println("position: X " + positions.get(i).getCoordinateX() + " Y " + positions.get(i).getCoordinateY());
    //         Enemy enemy = new Enemy(positions.get(i));
    //
    //         if (enemy != null)
    //             this.enemies.add(enemy);
    //     }
    //
    //     return;
    // }

    public void setEnemyPositions(int mapSize)
    {
        if (this.player.getPosition() == null)
        {
            int startPosition       = this.gameMap.getMapMiddle(this.player.getLevel()); //Map.getMapMiddle(this.level);

            this.player.setPosition(new Position(startPosition, startPosition));
        }

        for (int y = 1; y <= mapSize; y++)
        {
            for (int x = 1; x <= mapSize; x++)
            {
                Enemy enemy = new Enemy(new Position(x, y));
                this.enemies.add(enemy);
            }
        }

        removeEnemy(this.player.getPosition().getCoordinateX(), this.player.getPosition().getCoordinateY());

        int numEnemies = (int)(Math.round(Math.pow(mapSize, 2) / 2));
        int removeEnemiesNum = (int)(Math.pow(mapSize, 2) - numEnemies);

        int coordX;
        int coordY;

        while (removeEnemiesNum > 0)
        {
            coordX = this.randomGen(1, mapSize);
            coordY = this.randomGen(1, mapSize);

            if (this.removeEnemy(coordX, coordY))
                removeEnemiesNum--;
        }
    }

    public Enemy fetchEnemy()
    {
        if (this.player.getPosition() != null)
        {
            int playerX                 = this.player.getPosition().getCoordinateX();
            int playerY                 = this.player.getPosition().getCoordinateY();

            for (int i = 0; i < this.enemies.size(); i++)
            {
                Position enemPos = this.enemies.get(i).getPosition();
                if (enemPos.getCoordinateX() == playerX && enemPos.getCoordinateY() == playerY)
                    return this.enemies.get(i);
            }
        }
        return null;
    }

    // this removes enemies from specific positions and returns true or false if an enemy existed
    private boolean removeEnemy(int x, int y)
    {
        for (int i = 0; i < this.enemies.size(); i++)
        {
            Position enemPos = this.enemies.get(i).getPosition();
            if (enemPos.getCoordinateX() == x && enemPos.getCoordinateY() == y)
            {
                this.enemies.remove(this.enemies.get(i));
                return true;
            }
        }

        return false;
    }

    public void removeEnemy(Enemy enemy)
    {
        this.enemies.remove(enemy);
        return;
    }

    public boolean combat(Enemy counterEnemy) throws SQLException, IOException, BadLocationException, InterruptedException {
        GameController gamecontroller = Main.gameController;

        String blue     = "co:blue";
        String green    = "co:green";
        String red      = "co:red";
        String grey     = "co:grey";
        String reset    = "co:reset";

        @NotEmpty(message = "Enemy cannot be empty")
        Enemy enemy = counterEnemy;

        if (randomGen(1, 100) % 2 == 0)
        {
            gamecontroller.controllerStringMedium("You launch at the enemy with an attack!", "normal");

            this.player.giveDamage(enemy);
            gamecontroller.controllerStringMedium("Enemy is taking damage!", "normal");

            if (enemy.getHitPoints() <= 0)
            {
                gamecontroller.controllerStringMedium("----------------------------------------------[ " + red + "ENEMY" + " | " + grey + "HP: " +reset + enemy.getHitPoints() +" ]----------------------------------------------", "normal");
                gamecontroller.controllerStringMedium("The enemy hits the ground! ", "normal");
                gamecontroller.controllerStringMedium(("--- " + green + "SUCCESS" + reset + "! ---"), "normal");

                if (enemy.getDropLife() > 0 && enemy.getDropLife() > 0)
                {
                    String gained = (blue + "Gained | " + reset);
                    if (enemy.getDropLife() > 0)
                        gained += (grey + "Life : " + reset + enemy.getDropLife() + blue + " | " + reset);
                    if (enemy.getDropXP() > 0)
                        gained += (grey + "XP : " + reset + enemy.getDropXP());

                    gamecontroller.controllerStringMedium(gained, "normal");

                    this.player.gainLife(enemy.getDropLife());
                    this.player.gainExperience(enemy.getDropXP());
                }

                removeEnemy(enemy);
                return true; // someoneDies
            }

            return false;
        }
        else
        {

            gamecontroller.controllerStringMedium("ENEMY SWINGS!", "normal");

            if (randomGen(1, 100) % 2 == 0)
            {
                gamecontroller.controllerStringMedium("AND lands a blow", "normal");

                enemy.giveDamage(this.player);

                if (this.player.getHitPoints() <= 0)
                {
                    gamecontroller.controllerStringMedium(("--- " + red + "FAIL" + reset + "! ---"), "error");

                    gamecontroller.controllerStringMedium("you took too much damage!", "error");

                    gamecontroller.controllerStringMedium(("you are " + red + "DEAD" + reset), "error");

                    gamecontroller.controllerStringMedium("", "error");

                    this.player.characterDies();
                    return true; // someoneDies
                }

                gamecontroller.controllerStringMedium("you take " + red + enemy.getAttackPoints() + reset + " damage", "error");

                return false;
            }
            else
            {
                gamecontroller.controllerStringMedium("... and misses you", "normal");
                return false;
            }
        }
    }

    // MISC
    public int randomGen(int min, int max)
    {
        Random rand = new Random();

        return rand.nextInt(max - min + 1) + min;
    }

}

package za.co.wethinkcode.model.character;

import java.sql.SQLException;

public interface Character
{
    public void giveDamage(Character victim) throws SQLException;
    public void takeDamage(int damage, Character attacker) throws SQLException;
}

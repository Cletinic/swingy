package za.co.wethinkcode.model.character.heros;

import za.co.wethinkcode.model.character.Character;
import za.co.wethinkcode.Main;
import za.co.wethinkcode.model.GameModel;
import za.co.wethinkcode.model.Position;

import java.io.IOException;
import java.sql.SQLException;
import javax.swing.text.BadLocationException;
import javax.validation.constraints.*;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public abstract class Hero implements Character
{
    @NotEmpty(message = "Name cannot be empty")
    protected String    name;

    @Min(value = 1, message = "Level is out of bounds") // hardcoded
    @Max(value = 150, message = "Max level is 5 - You have managed to escape") // hardcoded
    protected int       level;

    @Min(value = 0, message = "Experience is out of bounds") // hardcoded
    @Max(value = 12200, message = "Max Experience is 12200 - You have managed to escape") // hardcoded
    protected int       experience;

    protected int       baseHP;
    protected int       baseAP;
    protected int       baseDP;

    protected int       hitPoints;
    protected int       attackPoints;
    protected int       defencePoints;

    // protected Item weapon;
    // protected Item armour;
    // protected Item helm;

    // @Size(min = 9, max = 29, message = "Map out of bounds")
    protected Position  position;
    protected Position  previousPosition;

    // // public void pickUpItem();
    // // public void gainXP(int gainedXP);

    /*
        - rogue -
            private int baseHP = 100 + 25;
            private int baseAP = 100;
            private int baseDP = 50 ;

        - knight -
            private int baseHP = 100;
            private int baseAP = 100;
            private int baseDP = 50 + 25;
        - bezerker -
            private int baseHP = 100 ;
            private int baseAP = 100 + 25;
            private int baseDP = 50;
     */

    public void gainExperience(int xp) throws SQLException, IOException, BadLocationException, InterruptedException {
        GameModel gameModel = Main.gameModel;

        int currentLevel    = this.level;

        if ((this.experience + xp) >= 12200)
        {
            gameModel.endGame("win");
            return;
        }

        this.experience += xp;

        this.level = gameModel.workoutLevel(this.experience);

        if (currentLevel < this.level)
            gameModel.gameMap.expandMap();
        gameModel.saveProgress();

        return;
    }

    public void gainLife(int hp)
    {
        if ((this.hitPoints + hp) > baseHP)
            this.hitPoints = this.getBaseHP();
        else
            this.hitPoints += hp;

        return;
    }


    public void characterDies() throws SQLException, IOException, BadLocationException, InterruptedException {
        Main.gameModel.endGame("dead");
        return;
    }

    public void characterMovement(String direction) throws SQLException //throws InvalidMovement
    {
        this.previousPosition   = this.position;
        int newPositionX        = this.position.getCoordinateX();
        int newPositionY        = this.position.getCoordinateY();

        switch(direction.toLowerCase())
        {
            case "south":
            {
                newPositionY += 1;
                break;
            }
            case "east":
            {
                newPositionX += 1;
                break;
            }
            case "north":
            {
                newPositionY -= 1;
                break;
            }
            case "west":
            {
                newPositionX -=1;
                break;
            }
            // default:
            //     // code block
        }

        this.position = new Position(newPositionX, newPositionY);

        return;
    }

    @Override
    public void giveDamage(Character victim) throws SQLException
    {
        victim.takeDamage(this.attackPoints, this);
        return;
    }

    @Override
    public void takeDamage(int damage, Character attacker) throws SQLException
    {
        if ((this.defencePoints) - damage > 0)
        {
            this.defencePoints -= damage;
            return;
        }
        else
        {
            int hit = damage - this.defencePoints;
            this.defencePoints = 0;

            this.hitPoints -= hit;
        }
        //
        // if (this.hitPoints < 0)
        //     characterDies();
    }
}

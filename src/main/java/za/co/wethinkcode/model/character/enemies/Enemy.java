package za.co.wethinkcode.model.character.enemies;

import za.co.wethinkcode.Main;
import za.co.wethinkcode.model.GameModel;
import za.co.wethinkcode.model.Position;
import za.co.wethinkcode.model.character.Character;

// Custom Exceptions

import java.sql.SQLException;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Enemy implements Character
{
    protected int hitPoints;
    protected int attackPoints;
    protected int defencePoints;

    protected int level;
    protected int dropLife;
    protected int dropXP;

    protected Position position;

    public Enemy(Position enemyPosition)
    {
        GameModel gameModel = Main.gameModel;

        this.position       = enemyPosition;

        int gameLevel       = gameModel.player.getLevel(); //GameModel.player.getLevel();
        this.level          = gameModel.gameLevel[gameLevel];
        this.hitPoints      = gameModel.randomGen((level / 2), level);

        this.dropLife       = gameModel.randomGen(0, (hitPoints / 100));
        this.dropXP         = gameModel.randomGen((hitPoints/ 10), (hitPoints / 2));

        this.defencePoints  = 0;


        /*
            • level[0] = Level 1:  0 - 1000 XP      = (0 + 1) × 1000 + ((0 + 1) - 1)^2 × 450 = 1000
            • level[1] = Level 2:  1001 - 2450 XP   = (1 + 1) × 1000 + ((1 + 1) - 1)^2 × 450 = 2450
            • level[2] = Level 3:  2451 - 4800 XP   = (2 + 1) × 1000 + ((2 + 1) - 1)^2 × 450 = 4800
            • level[3] = Level 4:  4801 - 8050 XP   = (3 + 1) × 1000 + ((3 + 1) - 1)^2 × 450 = 8050
            • level[4] = Level 5:  8051 - 12200 XP  = (4 + 1) × 1000 + ((4 + 1) - 1)^2 × 450 = 12200 6100 1220
        */

    }

    @Override
    public void giveDamage(Character victim) throws SQLException
    {
        int attack = hitPoints / 100;
        if (attack == 0)
            attack = 2;
        this.attackPoints = Main.gameModel.randomGen(1, attack);

        victim.takeDamage(this.attackPoints, this);
        return;
    }

    @Override
    public void takeDamage(int damage, Character attacker) throws SQLException
    {
        // GameModel gameModel = Main.gameModel;

        this.hitPoints -= damage;

        if (this.hitPoints <= 0)
            this.hitPoints = 0;

    }
}

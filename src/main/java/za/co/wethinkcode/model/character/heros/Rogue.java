package za.co.wethinkcode.model.character.heros;

import za.co.wethinkcode.Main;
import za.co.wethinkcode.model.Position;

import java.sql.SQLException;

public class Rogue extends Hero
{

    public Rogue(String characterName)
    {
        this.baseHP = 100 + 25 + 50;
        this.baseAP = 100;
        this.baseDP = 0;

        this.name               = characterName;

        this.experience         = 0;
        this.level = 1;

        //stats
        this.hitPoints          = baseHP;
        this.attackPoints       = baseAP;
        this.defencePoints      = baseDP;

        this.previousPosition   = null;
        this.position           = null;
        // int startPosition       = Main.gameModel.gameMap.getMapMiddle(this.level); //Map.getMapMiddle(this.level);
        // this.position           = new Position(startPosition, startPosition);

        // Items
        // this.armour             = null;
        // this.helm               = null;
        // this.weapon             = null;
    }

    // Name|Alive Status|PositionX|PositionY|XP|HP|AP|DP|Armour|Helm|Weapon
    public Rogue(String characterName, int characterPositionX, int characterPositionY, int characterXP, int characterHP, int characterAP, int characterDP)
    {
        this.baseHP = 100 + 25 + 50;
        this.baseAP = 100;
        this.baseDP = 0;

        this.name           = characterName;

        //stats
        this.experience     = characterXP;
        this.level          = Main.gameModel.workoutLevel(this.experience); //workoutLevel(this.experience);

        this.hitPoints      = (baseHP < characterHP ? baseHP : characterHP);
        this.attackPoints   = (baseAP < characterAP ? baseAP : characterAP);
        this.defencePoints  = (baseDP < characterDP ? baseDP : characterDP);

        this.position       = new Position(characterPositionX, characterPositionY);

        // Items
        // this.armour         = ItemFactory.newItem(characterArmourType, characterArmourClass); // Armor - increases defense
        // this.helm           = ItemFactory.newItem(characterHelmType, characterHelmClass); // Helm - increases hit points
        // this.weapon         = ItemFactory.newItem(characterWeaponType, characterWeaponClass); // Weapon - increases the attack

    }
}

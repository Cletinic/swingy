package za.co.wethinkcode.view;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import za.co.wethinkcode.Main;
import za.co.wethinkcode.controller.GameController;
import za.co.wethinkcode.model.GameModel;
import za.co.wethinkcode.model.Map;
import za.co.wethinkcode.model.Position;
import za.co.wethinkcode.model.character.enemies.Enemy;
import za.co.wethinkcode.model.character.heros.Hero;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLDocument;


import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class GUIView implements GameView {

    private JFrame frame = new JFrame("Swingy");
    private JTextPane editorPane1;
    private JScrollPane editorScroll;
    private JFormattedTextField formattedTextField1;
    private JPanel panel1;

    private String green = "co:green";
    private String grey = "co:grey";
    private String red = "co:red";
    private String blue = "co:blue";
    private String purple = "co:purple";
    private String reset = "co:reset";

    String result = "";


    public GUIView() {

        editorPane1.setContentType("text/html");

        // Action enterInput = new AbstractAction() {
        //     @Override
        //     public void actionPerformed(ActionEvent e) {
        //         if (formattedTextField1.getText().isEmpty() || formattedTextField1.getText() == null)
        //             userInput = "";
        //         else
        //         {
        //             userInput = formattedTextField1.getText().toString();
        //             userInputted = true;
        //         }
        //         //System.out.println(userInput);
        //
        //         viewMessage(userInput, "none");
        //
        //         formattedTextField1.setValue("");
        //     }
        // };
        //
        // button1.addActionListener(enterInput);
        // formattedTextField1.addActionListener(enterInput);

        //when cross is clicked
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onClose();
            }
        });

        //on ESCAPE
        panel1.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onClose();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        //editorPane1.setEditable(false);

        editorPane1.setEditable(false);

        frame.setContentPane((this.panel1));
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        //frame.setUndecorated(true);

        frame.addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent e) {
                formattedTextField1.requestFocus();
            }
        });

        frame.pack();
        frame.setVisible(true);

    }

    @Override
    public void onClose() {
        //Main.gameController.controllerCloseGame("close");
        frame.dispose();
        return;
    }

    @Override
    public void viewSelectGame() 
    {
        this.viewMessage(("Would you like to " + blue + "LOAD" + reset + " an old game or start a " + blue + "NEW" + reset + " game?"), "normal");

        formattedTextField1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String userInputString = formattedTextField1.getText().toString().toLowerCase();
                userInputString = userInputString.trim();
                formattedTextField1.setValue("");

                if (!userInputString.isEmpty() && userInputString != null) {
                    viewMessage(userInputString, "none");

                    if (userInputString.contains("load")) {
                        viewLoadGame();
                        return;
                    } else if (userInputString.contains("new")) {
                        viewNewGame();
                        return;
                    } else if (userInputString.contains("close")) {
                        try {
                            Main.gameController.controllerCloseGame("close no save");
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        } catch (BadLocationException ex) {
                            ex.printStackTrace();
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    } else if (userInputString.toLowerCase().contains("swap")) {
                        Main.gameController.controllerSwitchView("viewselectgame");
                        return;
                    }

                    viewMessage("Invalid Input", "error");
                }
            }
        });


        /*
            boolean validInput = false;

        do
        {
            this.viewMessage(("Would you like to "+ blue + "LOAD" + reset + " an old game or start a " + blue + "NEW" + reset + " game?"), "normal");

            String userInputString = userInput.nextLine();

            if (userInputString.toLowerCase().contains("load"))
            {
                validInput = true;
                this.viewLoadGame();
                break;
            }
            else if (userInputString.toLowerCase().contains("new"))
            {
                validInput = true;
                this.viewNewGame();
                break;
            }
            else if (userInputString.toLowerCase().contains("close"))
                Main.gameController.controllerCloseGame("close no save");

            this.viewMessage("Invalid Input" , "error");
        }
        while (!validInput);
         */

    }

    @Override
    public void viewNewGame()
    {

        this.viewMessage("What is the name of this new hero?", "normal");

        formattedTextField1.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e) {
                String userInputString = formattedTextField1.getText().toString().toLowerCase();
                userInputString = userInputString.trim();
                formattedTextField1.setValue("");

                if (!userInputString.isEmpty() && userInputString != null) {
                    viewMessage(userInputString, "none");

                    if (userInputString.equals("back")) {
                        viewSelectGame();
                        return;
                    } else if (userInputString.equals("load")) {
                        viewLoadGame();
                    } else if (userInputString.equals("close")) {
                        try {
                            Main.gameController.controllerCloseGame("close no save");
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        } catch (BadLocationException ex) {
                            ex.printStackTrace();
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    } else if (userInputString.toLowerCase().contains("swap")) {
                        Main.gameController.controllerSwitchView("viewnewgame");
                        return;
                    }

                    try {
                        if (gameController.controllerSetNewGame(userInputString))
                            return;
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (BadLocationException ex) {
                        ex.printStackTrace();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }

                    viewMessage("Hero already exists!", "error");
                }
            }
        });

        // boolean validInput = false;
        //
        // do
        // {
        //     this.viewMessage("What is the name of this new hero?", "normal");
        //     String userInputString = userInput.nextLine();
        //
        //     if (userInputString.toLowerCase().equals("back"))
        //     {
        //         validInput = true;
        //         this.viewSelectGame();
        //         break;
        //     }
        //     else if (userInputString.toLowerCase().equals("load"))
        //     {
        //         validInput = true;
        //         this.viewLoadGame();
        //         break;
        //     }
        //     else if (userInputString.toLowerCase().contains("close"))
        //         Main.gameController.controllerCloseGame("close no save");
        //
        //     if (gameController.controllerSetNewGame(userInputString))
        //         return;
        //
        //     this.viewMessage("Hero already exists!", "error");
        // }
        // while (!validInput);
    }

    @Override
    public void viewLoadGame() 
    {

        try {
            if (viewFormatSaves(gameController.controllerGetSaves()))
                viewMessage("What is the name of the hero you would like to play?", "normal");
            else {
                viewMessage("No saved files have been found!", "error");
                viewMessage("Let's make a new Hero!", "normal");
                viewNewGame();
                return;
            }

            formattedTextField1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        String userInputString = formattedTextField1.getText().toString().toLowerCase();
                        userInputString.replaceAll("\\p{Blank}", "");
                        formattedTextField1.setValue("");

                        if (!userInputString.isEmpty() && userInputString != null) {
                            viewMessage(userInputString, "none");

                            if (userInputString.equals("new")) {
                                viewNewGame();
                                return;
                            } else if (userInputString.contains("close"))
                                Main.gameController.controllerCloseGame("close no save");
                            else if (userInputString.toLowerCase().contains("swap")) {
                                Main.gameController.controllerSwitchView("viewloadgame");
                                return;
                            }
                            if (gameController.controllerFetchSave(userInputString))
                                return;
                            else {
                                viewMessage(("Unable to find a hero with the name \"" + blue + userInputString + reset + "\""), "error");
                            }
                        }

                        //this.viewSelectGame();
                        //return;
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (BadLocationException ex) {
                        ex.printStackTrace();
                    } catch (InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }
            });
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // boolean validInput = false;

        // do
        // {
        //     if (this.viewFormatSaves(gameController.controllerGetSaves()))
        //     {
        //
        //         this.viewMessage("What is the name of the hero you would like to play?", "normal");
        //         String userInputString = userInput.nextLine();
        //
        //         if (userInputString.toLowerCase().equals("new"))
        //         {
        //             this.viewNewGame();
        //             break;
        //         }
        //         else if (userInputString.toLowerCase().contains("close"))
        //             Main.gameController.controllerCloseGame("close no save");
        //
        //         if (gameController.controllerFetchSave(userInputString))
        //             break;
        //         else
        //             this.viewMessage(("Unable to find a hero with the name \"" + blue + userInputString + reset + "\"" ), "error");
        //     }
        //     else
        //     {
        //
        //         this.viewMessage("No saved files have been found!", "error");
        //         this.viewMessage("Let's make a new Hero!", "normal");
        //         this.viewNewGame();
        //         return;
        //     }
        //
        //     validInput = true;
        //     this.viewSelectGame();
        //     break;
        //
        // }
        // while (!validInput);

    }

    @Override
    public void viewGameMovement() 
    {
        this.viewTopBanner();
        this.viewMap();
        this.viewMessage(("Do you want to go " + blue + "NORTH" + reset + ", " + blue + "SOUTH" + reset + ", " + blue + "EAST" + reset + " or " + blue + "WEST" + reset + "?"), "normal");

        formattedTextField1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String userInputString = formattedTextField1.getText().toString().toLowerCase();
                userInputString = userInputString.trim();
                formattedTextField1.setValue("");

                if (!userInputString.isEmpty() && userInputString != null) {
                    viewMessage(userInputString, "none");

                    if (userInputString.contains("north") | userInputString.contains("south") || userInputString.contains("east") || userInputString.contains("west")) {
                        // 0 for no encounter
                        // 1 for enemy encounter
                        // 2 for wall encounter
                        // 3 win encounter

                        int encounter = 0;
                        try {
                            encounter = Main.gameController.controllerMovement(userInputString);
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        } catch (BadLocationException ex) {
                            ex.printStackTrace();
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }

                        if (encounter == 0)
                            viewMessage("\nYou take a step " + blue + userInputString.toLowerCase() + reset + "\n", "normal");
                        else if (encounter == 1) {
                            viewMessage("\nYou have encountered an enemy\n", "normal");
                            viewGameEncounter();
                            return;
                        }
                        // else if (encounter == 2)
                        // {
                        //     viewMessage("You hit your face into a wall", "error");
                        //     continue;
                        // }
                        // else if (encounter == 3)
                        // {
                        //     viewMessage("\nYou have won the game\n", "normal");
                        //     viewWinGame();
                        //     return;
                        // }
                    } else if (userInputString.contains("close")) {
                        try {
                            Main.gameController.controllerCloseGame("close");
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        } catch (BadLocationException ex) {
                            ex.printStackTrace();
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                        return;
                    } else if (userInputString.toLowerCase().contains("swap")) {
                        Main.gameController.controllerSwitchView("viewgamemovement");
                        return;
                    } else {
                        viewMessage("Invalid Input (movement)", "error");
                    }
                    viewGameMovement();
                }
            }
        });

        // userInput = new Scanner(System.in);
        //
        // boolean validInput = false;
        //
        // do {
        //     this.viewTopBanner();
        //     this.viewMap();
        //     this.viewMessage(("Do you want to go " + blue + "NORTH" + reset + ", " + blue + "SOUTH" + reset + ", " + blue + "EAST" + reset + " or " + blue + "WEST" + reset + "?"), "normal");
        //     String userInputString = userInput.nextLine().toLowerCase();
        //
        //     if (userInputString.contains("north") | userInputString.contains("south") || userInputString.contains("east") || userInputString.contains("west")) {
        //         // 0 for no encounter
        //         // 1 for enemy encounter
        //         // 2 for wall encounter
        //         // 3 win encounter
        //
        //         int encounter = Main.gameController.controllerMovement(userInputString);
        //
        //         if (encounter == 0)
        //         {
        //             this.viewMessage("\nYou take a step " + blue + userInputString.toLowerCase() + reset + "\n", "normal");
        //             continue;
        //         }
        //         else if (encounter == 1) {
        //             this.viewMessage("\nYou have encountered an enemy\n", "normal");
        //             this.viewGameEncounter();
        //             continue;
        //         }
        //         // else if (encounter == 2)
        //         // {
        //         //     viewMessage("You hit your face into a wall", "error");
        //         //     continue;
        //         // }
        //         // else if (encounter == 3)
        //         // {
        //         //     viewMessage("\nYou have won the game\n", "normal");
        //         //     viewWinGame();
        //         //     return;
        //         // }
        //     }
        //     else if (userInputString.toLowerCase().contains("close"))
        //     {
        //         Main.gameController.controllerCloseGame("close");
        //         return;
        //     }
        //     else
        //     {
        //         this.viewMessage("Invalid Input", "error");
        //     }
        //
        //     //this.viewBottomBanner();
        // }
        // while (!validInput);

    }

    @Override
    public void viewGameEncounter() 
    {
        GameController gameController = Main.gameController;

        Enemy enemy = gameController.controllerFetchEnemy();

        this.viewTopBanner();
        this.viewEnemyBanner(enemy);

        this.viewMessage("Will you " + blue + "FIGHT" + reset + " or try to " + blue + "RUN" + reset + "??????????????????????", "normal");

        formattedTextField1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String userInputString = formattedTextField1.getText().toString().toLowerCase();
                userInputString = userInputString.trim();
                formattedTextField1.setValue("");

                if (!userInputString.isEmpty() && userInputString != null) {
                    viewMessage(userInputString, "none");

                    if (userInputString.contains("run")) {
                        viewMessage("You try escape...", "normal");

                        if (gameController.controllerAttemptRun()) {
                            viewMessage(("--- " + green + "SUCCESS" + reset + "! ---"), "normal");
                            viewMessage("you escaped!", "normal");
                            viewMessage("You are now where you were, before you moved.", "normal");
                            viewGameMovement();
                            return;
                        }

                        viewMessage(("--- " + red + "FAIL" + reset + "! ---"), "error");
                        viewMessage("... your enemy has blocked your escape!", "error");
                        viewMessage("You have to fight", "error");

                        try {
                            if (gameController.controllerCombat(enemy)) {
                                viewGameMovement();
                                return;
                            }
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        } catch (BadLocationException ex) {
                            ex.printStackTrace();
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                    } else if (userInputString.equals("fight")) {
                        viewMessage("You choose to fight!", "normal");

                        try {
                            if (gameController.controllerCombat(enemy)) {
                                viewGameMovement();
                                return;
                            }
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        } catch (BadLocationException ex) {
                            ex.printStackTrace();
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }

                    } else if (userInputString.equals("auto")) {
                        viewGameFightAuto();
                        return;
                    } else if (userInputString.contains("close")) {
                        try {
                            Main.gameController.controllerCloseGame("close");
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        } catch (BadLocationException ex) {
                            ex.printStackTrace();
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                        return;
                    } else if (userInputString.toLowerCase().contains("swap")) {
                        Main.gameController.controllerSwitchView("viewgameencounter");
                        return;
                    } else
                        viewMessage("Invalid Input (encounter)", "error");
                    viewGameEncounter();
                }
            }
        });

        // GameController gameController = Main.gameController;
        //
        // boolean someoneDies = false;
        // Enemy enemy = gameController.controllerFetchEnemy();
        //
        // this.viewMessage("You have encountered an enemy!", "normal");
        //
        // while (!someoneDies)
        // {
        //     this.viewTopBanner();
        //     this.viewEnemyBanner(enemy);
        //
        //     this.viewMessage("Will you " + blue + "FIGHT" + reset + " or try to " + blue + "RUN" + reset + "??????????????????????", "normal");
        //
        //     String userInputString = userInput.nextLine();
        //
        //     if (userInputString.toLowerCase().equals("run"))
        //     {
        //         this.viewMessage("You try escape...", "normal");
        //
        //         if (gameController.controllerAttemptRun())
        //         {
        //             this.viewMessage(("--- " + green + "SUCCESS" + reset + "! ---"), "normal");
        //             this.viewMessage("you escaped!", "normal");
        //             this.viewMessage("You are now where you were, before you moved.", "normal");
        //             return;
        //         }
        //
        //         this.viewMessage(("--- " + red + "FAIL" + reset + "! ---"), "error");
        //         this.viewMessage("... your enemy has blocked your escape!", "error");
        //         this.viewMessage("You have to fight", "error");
        //
        //         someoneDies = gameController.controllerCombat(enemy);
        //     }
        //     else if (userInputString.toLowerCase().equals("fight"))
        //     {
        //         this.viewMessage("You choose to fight!", "normal");
        //
        //         someoneDies = gameController.controllerCombat(enemy);
        //
        //     }
        //     else if (userInputString.toLowerCase().equals("auto"))
        //     {
        //         this.viewGameFightAuto();
        //         someoneDies = true;
        //     }
        //     else if (userInputString.toLowerCase().contains("close"))
        //     {
        //         Main.gameController.controllerCloseGame("close");
        //         return;
        //     }
        //     else
        //         this.viewMessage("Invalid Input", "error");
        //
        //     if (someoneDies)
        //         return;
        // }

    }

    private void viewGameFightAuto() 
    {

        this.viewMessage("Fighting in AUTO means you lose the ability to run mid-battle", "normal");
        this.viewMessage(("Fight in AUTO? Answer " + blue + "YES" + reset + " or " + blue + "NO" + reset), "normal");

        formattedTextField1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String userInputString = formattedTextField1.getText().toString().toLowerCase();
                userInputString.replaceAll("\\p{Blank}", "");
                formattedTextField1.setValue("");

                if (!userInputString.isEmpty() && userInputString != null) {
                    viewMessage(userInputString, "none");

                    if (userInputString.equals("yes")) {
                        viewGameFight();
                        viewGameMovement();
                        return;
                    } else if (userInputString.equals("no")) {
                        viewGameEncounter();
                        return;
                    } else if (userInputString.contains("close")) {
                        try {
                            Main.gameController.controllerCloseGame("close");
                        } catch (SQLException ex) {
                            ex.printStackTrace();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        } catch (BadLocationException ex) {
                            ex.printStackTrace();
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }
                        return;
                    }
                    viewMessage("Invalid Input (game auto)", "error");
                }
            }
        });

        // boolean validInput = false;
        //
        // while (!validInput)
        // {
        //     this.viewMessage("Fighting in AUTO means you lose the ability to run mid-battle", "normal");
        //     this.viewMessage(("Fight in AUTO? Answer " + blue + "YES" + reset + " or " + blue + "NO" + reset), "normal");
        //     String userInputString = userInput.nextLine();
        //
        //     if (userInputString.toLowerCase().equals("yes"))
        //     {
        //         this.viewGameFight();
        //         validInput = true;
        //         return;
        //     }
        //     else if (userInputString.toLowerCase().equals("no"))
        //     {
        //         this.viewGameEncounter();
        //         validInput = true;
        //         return;
        //     }
        //     else if (userInputString.toLowerCase().contains("close"))
        //     {
        //         Main.gameController.controllerCloseGame("close");
        //         return;
        //     }
        //     this.viewMessage("Invalid Input", "error");
        // }
    }

    private void viewGameFight() 
    {
        try {
            GameController gameController = Main.gameController;

            boolean someoneDies = false;
            Enemy enemy = gameController.controllerFetchEnemy();

            while (!someoneDies) {
                this.viewTopBanner();
                this.viewEnemyBanner(enemy);

                if (gameController.controllerCombat(enemy)) {
                    // System.out.println("game fight: return");
                    return;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BadLocationException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void viewEndGame()
    {
        Main.gameController.controllerSwitchView("viewendgame");
        return;
    }

    @Override
    public void viewWinGame()
    {
        Main.gameController.controllerSwitchView("viewwingame");
        return;
    }

    @Override
    public void viewMessage(String message, String type) {
        editorPane1.setEditable(true);

        String green = "style=\"color: rgb(162,184,36); font-weight: bold; font-family: Consolas, monaco, monospace;\"";
        String grey = "style=\"color: rgb(120,120,120); font-weight: bold; font-family: Consolas, monaco, monospace;\"";
        String red = "style=\"color: rgb(255,107,104); font-weight: bold; font-family: Consolas, monaco, monospace;\"";
        String blue = "style=\"color: rgb(83,148,236); font-weight: bold; font-family: Consolas, monaco, monospace;\"";
        String purple = "style=\"color: rgb(177, 79, 209); font-weight: bold; font-family: Consolas, monaco, monospace;\"";
        String reset = "style=\"color: rgb(0,0,0); font-weight: bold; font-family: Consolas, monaco, monospace;\"";

        String text;
        String mesType = "";
        String replaceString = message;

        if (type.toLowerCase().contains("error") || type.toLowerCase().contains("normal"))
            mesType = (type.toLowerCase().contains("error") ? ("[<span " + red + ">" + "ERROR" + "</span>" + "] ") : ("[<span " + blue + ">" + "SWINGY" + "</span>" + "] "));

        replaceString = replaceString.replace("co:blue", ("<span " + blue + ">"));
        replaceString = replaceString.replace("co:grey", ("<span " + grey + ">"));
        replaceString = replaceString.replace("co:green", ("<span " + green + ">"));
        replaceString = replaceString.replace("co:purple", ("<span " + purple + ">"));
        replaceString = replaceString.replace("co:red", ("<span " + red + ">"));
        replaceString = replaceString.replace("co:reset", "</span>");


        text = ("<p" + reset + ">" + mesType + replaceString + "</p><br>");

        this.appendToJPane(text);

    }

    private void appendToJPane(String text) {

        try {
            HTMLDocument doc = (HTMLDocument) editorPane1.getStyledDocument();
            doc.insertAfterEnd(doc.getCharacterElement(doc.getLength()), text);

        } catch (BadLocationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        editorPane1.setEditable(false);
        return;
    }


    @Override
    public void viewMap() {
        GameModel gameModel = Main.gameController.controllerReturnModel();
        Map gameMap = gameModel.gameMap;
        Hero player = gameModel.player;

        String table = "<br><table>";
        String member;
        int mapSize = gameMap.mapSize;


        for (int y = 1; y <= mapSize; y++) {
            table += "<tr>";
            for (int x = 1; x <= mapSize; x++) {
                member = "<td style=\"border: 1px solid black; border-collapse: collapse;\">";
                String entity = "";
                if (player.getPosition().getCoordinateX() == x && player.getPosition().getCoordinateY() == y)
                    entity += (blue + "*" + reset);
                for (int i = 0; i < gameModel.enemies.size(); i++) {
                    Position enemPos = gameModel.enemies.get(i).getPosition();
                    if (enemPos.getCoordinateX() == x && enemPos.getCoordinateY() == y)
                        entity += (red + "*" + reset);
                    if (player.getPosition().getCoordinateX() == x && player.getPosition().getCoordinateY() == y &&
                            enemPos.getCoordinateX() == x && enemPos.getCoordinateY() == y)
                        entity = (purple + "*" + reset);
                }
                if (entity.length() == 0)
                    entity = "  ";
                member += entity;
                member += "</td>";
                table += member;
            }

            if (y != mapSize - 1)
                table += "</tr>";
        }

        table += "</table>";

        viewMessage(table, "normal");
    }

    @Override
    public void viewTopBanner() {
        Hero hero = gameController.controllerReturnModel().player;
        int heroLevel = gameController.controllerReturnModel().workoutLevel(hero.getExperience());

        this.viewMessage(("--------------------------------[ " + blue + hero.getName().toUpperCase() + blue + " |" + grey + " Level: " + reset + heroLevel + blue + " |" + grey + " XP: " + reset + hero.getExperience() + blue + " | " + grey + "HP: " + reset + hero.getHitPoints() + " ]---------------------------------"), "normal");
    }

    @Override
    public void viewBottomBanner() {
        this.viewMessage("------------------------------------------------------------------------------------------------------------------", "normal");
    }

    @Override
    public void viewEnemyBanner(Enemy enemy) {
        this.viewMessage("----------------------------------------------[ " + red + "ENEMY" + " | " + grey + "HP: " + reset + enemy.getHitPoints() + " ]----------------------------------------------", "normal");
        return;
    }

    @Override
    public boolean viewFormatSaves(List<Hero> saves) {
        if (saves.isEmpty())
            return false;

        this.viewMessage("-----------------------------------------------------[ " + blue + "Saves" + reset + " ]----------------------------------------------------", "normal");

        for (int i = 0; i < saves.size(); i++) {
            Hero hero = saves.get(i);

            int heroLevel = gameController.controllerReturnModel().workoutLevel(hero.getExperience());

            // System.out.println("characterName : " +  hero.getName());
            // System.out.println("characterPosX : " +  hero.getPosition().getCoordinateX());
            // System.out.println("characterPosY : " +  hero.getPosition().getCoordinateY());
            // System.out.println("characterXP : " +  hero.getExperience());
            // System.out.println("characterHP : " +  hero.getHitPoints());
            // System.out.println("characterAP: " +  hero.getAttackPoints());
            // System.out.println("characterDP: " +  hero.getDefencePoints());

            String savedHero = "    " + blue + hero.getName().toUpperCase() + blue + "      |" + grey + " Level: " + reset + heroLevel + blue + " |" + grey + " XP: " + reset + hero.getExperience() + blue + " | " + grey + "HP: " + reset + hero.getHitPoints() + "   ";
            this.viewMessage(savedHero, "normal");

        }
        this.viewMessage("------------------------------------------------------------------------------------------------------------------", "normal");
        return true;
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(2, 2, new Insets(0, 0, 0, 0), -1, -1));
        editorScroll = new JScrollPane();
        panel1.add(editorScroll, new GridConstraints(0, 0, 1, 2, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        editorPane1 = new JTextPane();
        editorScroll.setViewportView(editorPane1);
        formattedTextField1 = new JFormattedTextField();
        panel1.add(formattedTextField1, new GridConstraints(1, 0, 1, 2, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel1;
    }

}


    /*


import za.co.wethinkcode.model.character.enemies.Enemy;
import za.co.wethinkcode.model.character.heros.Hero;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.html.HTMLEditorKit;
import java.io.IOException;
import java.io.StringReader;
import java.sql.SQLException;
import java.util.List;

public class GUIView extends JDialog implements GameView
{

    public GUIView()
    {

        // JFrame frame = new JFrame("Swingy");
        // frame.setContentPane(contentPane);
        // frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        // frame.setVisible(true);

        // frame = new JFrame("Swingy");;
        // frame.setContentPane(contentPane);
        // setModal(true);
        //
        // frame.setVisible(true);

        // Action enterInput = new AbstractAction()
        // {
        //     @Override
        //     public void actionPerformed(ActionEvent e)
        //     {
        //         System.out.println("some action");
        //     }
        // };
        //
        // getRootPane().setDefaultButton(buttonEnter);
        // buttonEnter.addActionListener(enterInput);
        // inputArea.addActionListener(enterInput);
        //
        // //when cross is clicked
        // setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        // addWindowListener(new WindowAdapter() {
        //     public void windowClosing(WindowEvent e) {
        //         onClose();
        //     }
        // });
        //
        // //on ESCAPE
        // contentPane.registerKeyboardAction(new ActionListener(){
        //     public void actionPerformed(ActionEvent e)
        //     {
        //         onClose();
        //     }
        // }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);


        // frame.setVisible(true);
    }

    private void onClose()
    {

        dispose();
    }

    private void appendToJPane(String text) throws IOException, BadLocationException
    {
        dispalyPane.setContentType("text/html");
        HTMLEditorKit kit = new HTMLEditorKit();
        dispalyPane.setEditorKit(kit);

        StringReader reader = new StringReader(text);

        kit.read(reader, dispalyPane.getDocument(), dispalyPane.getDocument().getLength());
    }

    @Override
    public void viewSelectGame() throws SQLException {

    }

    @Override
    public void viewNewGame() throws SQLException {

    }

    @Override
    public void viewLoadGame() throws SQLException {

    }

    @Override
    public void viewGameMovement() throws SQLException {

    }

    @Override
    public void viewGameEncounter() throws SQLException {

    }

    @Override
    public void viewEndGame() {

    }

    @Override
    public void viewWinGame() {

    }

    @Override
    public void viewMessage(String message, String type) throws IOException, BadLocationException
    {
        //        System.out.println(type == "error" ? ("[" + red + "ERROR" + reset + "] " + message) : ("[" + blue + "SWINGY" + reset + "] " + message));
        String text = (type == "error" ? ("[" + "" + "ERROR" + "" + "] " + message) : ("[" + "" + "SWINGY" + "" + "] " + message));
        this.appendToJPane(text);
    }

    @Override
    public void viewMap() {

    }

    @Override
    public void viewTopBanner() {

    }

    @Override
    public void viewBottomBanner() {

    }

    @Override
    public void viewEnemyBanner(Enemy enemy) {

    }

    @Override
    public boolean viewFormatSaves(List<Hero> saves) {
        return false;
    }
}


/*
package za.co.wethinkcode.view;

import za.co.wethinkcode.model.character.enemies.Enemy;
import za.co.wethinkcode.model.character.heros.Hero;

import java.util.List;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.*;

public class GUIView implements GameView
{

    private JPanel panel1;

    @Override
    public void viewSelectGame() {

    }

    @Override
    public void viewNewGame() {

    }

    @Override
    public void viewLoadGame() {

    }

    @Override
    public void viewGameMovement() {

    }

    @Override
    public void viewGameEncounter() {

    }

    @Override
    public void viewEndGame() {

    }

    @Override
    public void viewWinGame() {

    }

    @Override
    public void viewMessage(String message, String type) {

    }

    @Override
    public void viewMap() {

    }

    @Override
    public void viewTopBanner() {

    }

    @Override
    public void viewBottomBanner() {

    }

    @Override
    public void viewEnemyBanner(Enemy enemy) {

    }

    @Override
    public boolean viewFormatSaves(List<Hero> saves) {
        return false;
    }

    public static void makeView()
    {
        JFrame f = new JFrame();//creating instance of JFrame

        JButton b = new JButton("click");//creating instance of JButton
        b.setBounds(130, 100, 100, 40);//x axis, y axis, width, height

        f.add(b);//adding button in JFrame

        f.setSize(400, 500);//400 width and 500 height
        f.setLayout(null);//using no layout managers
        f.setVisible(true);//making the frame visible
    }

}


// public static void makeView()
//         {
//         JFrame f = new JFrame();//creating instance of JFrame
//
//         JButton b = new JButton("click");//creating instance of JButton
//         b.setBounds(130, 100, 100, 40);//x axis, y axis, width, height
//
//         f.add(b);//adding button in JFrame
//
//         f.setSize(400, 500);//400 width and 500 height
//         f.setLayout(null);//using no layout managers
//         f.setVisible(true);//making the frame visible
//         }
 */




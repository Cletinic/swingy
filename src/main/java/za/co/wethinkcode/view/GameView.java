package za.co.wethinkcode.view;

import za.co.wethinkcode.Main;
import za.co.wethinkcode.controller.GameController;
import za.co.wethinkcode.model.character.enemies.Enemy;
import za.co.wethinkcode.model.character.heros.Hero;

import javax.swing.text.BadLocationException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public interface GameView
{
    public GameController gameController = Main.gameController;

    public void viewSelectGame() throws SQLException, IOException, BadLocationException, InterruptedException;

    public void viewNewGame() throws SQLException, IOException, BadLocationException, InterruptedException;
    public void viewLoadGame() throws SQLException, IOException, BadLocationException, InterruptedException;

    public void viewGameMovement() throws SQLException, IOException, BadLocationException, InterruptedException;
    public void viewGameEncounter() throws SQLException, IOException, BadLocationException, InterruptedException;

    // private void viewGameFightAuto();
    // private void viewGameFight();

    public void viewEndGame() throws IOException, BadLocationException;
    public void viewWinGame() throws IOException, BadLocationException;

    public void viewMessage(String message, String type) throws IOException, BadLocationException;

    public void viewMap() throws IOException, BadLocationException;

    public void viewTopBanner();
    public void viewBottomBanner();
    public void viewEnemyBanner(Enemy enemy);

    public boolean viewFormatSaves(List<Hero> saves);

    public void onClose();

}

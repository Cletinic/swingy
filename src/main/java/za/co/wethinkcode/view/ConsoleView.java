package za.co.wethinkcode.view;

import za.co.wethinkcode.Main;
import za.co.wethinkcode.controller.GameController;
import za.co.wethinkcode.model.GameModel;
import za.co.wethinkcode.model.Map;
import za.co.wethinkcode.model.Position;
import za.co.wethinkcode.model.character.enemies.Enemy;
import za.co.wethinkcode.model.character.heros.Hero;

import javax.swing.text.BadLocationException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class ConsoleView implements GameView
{
    private static Scanner userInput = new Scanner(System.in);

    // COLOURS
    private String green     = "\033[38;2;162;184;36m";
    private String grey     = "\033[38;2;120;120;120m";
    private String red      = "\033[38;2;255;107;104m";
    private String blue     = "\033[38;2;83;148;236m";
    private String purple   = "\033[38;2;169;125;250m";
    private String reset    = "\u001B[0m";

    /// START GAME
    public GameView viewInitialiseGame(String args[]) throws SQLException, IOException, BadLocationException, InterruptedException {
        if (args.length == 1)
        {
            if (args[0].toLowerCase().contains("gui"))
                return new GUIView();
            else if (args[0].toLowerCase().contains("console"))
                return new ConsoleView();
        }

        if (Main.gameView == null)
        {
            userInput = new Scanner(System.in);

            boolean validInput = false;

            do
            {
                this.viewMessage(("Would you like to play in " + blue + "CONSOLE" + reset + " mode or " + blue + "GUI" + reset + " mode?"), "normal");
                String userInputString = userInput.nextLine();

                if (userInputString.toLowerCase().contains("console"))
                    return new ConsoleView();
                else if (userInputString.toLowerCase().contains("gui"))
                    return new GUIView();
                else if (userInputString.toLowerCase().contains("close"))
                    Main.gameController.controllerCloseGame("close no save");

                this.viewMessage("Invalid Input", "error");
            }
            while (!validInput);
        }

        return new ConsoleView(); // if all else fails, just run it within the console.
    }

    @Override
    public void viewSelectGame() throws SQLException, IOException, BadLocationException, InterruptedException
    {
        boolean validInput = false;

        do
        {
            this.viewMessage(("Would you like to "+ blue + "LOAD" + reset + " an old game or start a " + blue + "NEW" + reset + " game?"), "normal");

            String userInputString = userInput.nextLine();

            if (userInputString.toLowerCase().contains("load"))
            {
                validInput = true;
                this.viewLoadGame();
                break;
            }
            else if (userInputString.toLowerCase().contains("new"))
            {
                validInput = true;
                this.viewNewGame();
                break;
            }
            else if (userInputString.toLowerCase().contains("close"))
                Main.gameController.controllerCloseGame("close no save");
            else if (userInputString.toLowerCase().contains("swap"))
            {
                Main.gameController.controllerSwitchView("viewselectgame");
                return;
            }

            this.viewMessage("Invalid Input" , "error");
        }
        while (!validInput);
    }

    @Override
    public void viewNewGame() throws SQLException, IOException, BadLocationException, InterruptedException {
        boolean validInput = false;

        do
        {
            this.viewMessage("What is the name of this new hero?", "normal");
            String userInputString = userInput.nextLine();
            userInputString = userInputString.trim(); //userInputString.replaceAll("\\s","");

            if (!userInputString.isEmpty() && userInputString != null)
            {

                if (userInputString.toLowerCase().equals("back")) {
                    validInput = true;
                    this.viewSelectGame();
                    break;
                } else if (userInputString.toLowerCase().equals("load")) {
                    validInput = true;
                    this.viewLoadGame();
                    break;
                } else if (userInputString.toLowerCase().contains("close"))
                    Main.gameController.controllerCloseGame("close no save");
                else if (userInputString.toLowerCase().contains("swap")) {
                    Main.gameController.controllerSwitchView("viewnewgame");
                    return;
                }

                if (gameController.controllerSetNewGame(userInputString))
                    return;

                this.viewMessage("Hero already exists!", "error");
            }
            this.viewMessage("Invalid Input", "error");
        }
        while (!validInput);
    }

    @Override
    public void viewLoadGame() throws SQLException, IOException, BadLocationException, InterruptedException {
        boolean validInput = false;

        do
        {
            if (this.viewFormatSaves(gameController.controllerGetSaves()))
            {
                this.viewMessage("What is the name of the hero you would like to play?", "normal");
                String userInputString = userInput.nextLine();

                if (userInputString.toLowerCase().equals("new"))
                {
                    this.viewNewGame();
                    break;
                }
                else if (userInputString.toLowerCase().contains("close"))
                    Main.gameController.controllerCloseGame("close no save");
                else if (userInputString.toLowerCase().contains("swap"))
                {
                    Main.gameController.controllerSwitchView("viewloadgame");
                    return;
                }

                if (gameController.controllerFetchSave(userInputString))
                    break;
                else
                    this.viewMessage(("Unable to find a hero with the name \"" + blue + userInputString + reset + "\"" ), "error");
            }
            else
            {

                this.viewMessage("No saved files have been found!", "error");
                this.viewMessage("Let's make a new Hero!", "normal");
                this.viewNewGame();
                return;
            }

            validInput = true;
            this.viewSelectGame();
            break;

        }
        while (!validInput);
    }

    @Override
    public void viewGameMovement() throws SQLException, IOException, BadLocationException, InterruptedException {
        userInput = new Scanner(System.in);

        boolean validInput = false;

        do {
            this.viewTopBanner();
            this.viewMap();
            this.viewMessage(("Do you want to go " + blue + "NORTH" + reset + ", " + blue + "SOUTH" + reset + ", " + blue + "EAST" + reset + " or " + blue + "WEST" + reset + "?"), "normal");
            String userInputString = userInput.nextLine().toLowerCase();

            if (userInputString.contains("north") | userInputString.contains("south") || userInputString.contains("east") || userInputString.contains("west")) {
                // 0 for no encounter
                // 1 for enemy encounter
                // 2 for wall encounter
                // 3 win encounter

                int encounter = Main.gameController.controllerMovement(userInputString);

                if (encounter == 0)
                {
                    this.viewMessage("\nYou take a step " + blue + userInputString.toLowerCase() + reset + "\n", "normal");
                    continue;
                }
                else if (encounter == 1) {
                    this.viewMessage("\nYou have encountered an enemy\n", "normal");
                    this.viewGameEncounter();
                    continue;
                }
                // else if (encounter == 2)
                // {
                //     viewMessage("You hit your face into a wall", "error");
                //     continue;
                // }
                // else if (encounter == 3)
                // {
                //     viewMessage("\nYou have won the game\n", "normal");
                //     viewWinGame();
                //     return;
                // }
            }
            else if (userInputString.toLowerCase().contains("close"))
            {
                Main.gameController.controllerCloseGame("close");
                return;
            }
            else if (userInputString.toLowerCase().contains("swap"))
            {
                Main.gameController.controllerSwitchView("viewgamemovement");
                return;
            }

            else
            {
                this.viewMessage("Invalid Input", "error");
            }


            //this.viewBottomBanner();
        }
        while (!validInput);
    }

    @Override
    public void viewGameEncounter() throws SQLException, IOException, BadLocationException, InterruptedException {
        GameController gameController = Main.gameController;

        boolean someoneDies = false;
        Enemy enemy = gameController.controllerFetchEnemy();

        while (!someoneDies)
        {
            this.viewTopBanner();
            this.viewEnemyBanner(enemy);

            this.viewMessage("Will you " + blue + "FIGHT" + reset + " or try to " + blue + "RUN" + reset + "??????????????????????", "normal");

            String userInputString = userInput.nextLine();

            if (userInputString.toLowerCase().equals("run"))
            {
                this.viewMessage("You try escape...", "normal");

                if (gameController.controllerAttemptRun())
                {
                    this.viewMessage(("--- " + green + "SUCCESS" + reset + "! ---"), "normal");
                    this.viewMessage("you escaped!", "normal");
                    this.viewMessage("You are now where you were, before you moved.", "normal");
                    return;
                }

                this.viewMessage(("--- " + red + "FAIL" + reset + "! ---"), "error");
                this.viewMessage("... your enemy has blocked your escape!", "error");
                this.viewMessage("You have to fight", "error");

                someoneDies = gameController.controllerCombat(enemy);
            }
            else if (userInputString.toLowerCase().equals("fight"))
            {
                this.viewMessage("You choose to fight!", "normal");

                someoneDies = gameController.controllerCombat(enemy);

            }
            else if (userInputString.toLowerCase().equals("auto"))
            {
                this.viewGameFightAuto();
                someoneDies = true;
            }
            else if (userInputString.toLowerCase().contains("close"))
            {
                Main.gameController.controllerCloseGame("close");
                return;
            }
            else if (userInputString.toLowerCase().contains("swap"))
            {
                Main.gameController.controllerSwitchView("viewgameencounter");
                return;
            }
            else
                this.viewMessage("Invalid Input", "error");

            if (someoneDies)
                return;
        }
    }

    private void viewGameFightAuto() throws SQLException, IOException, BadLocationException, InterruptedException {
        boolean validInput = false;

        while (!validInput)
        {
            this.viewMessage("Fighting in AUTO means you lose the ability to run mid-battle", "normal");
            this.viewMessage(("Fight in AUTO? Answer " + blue + "YES" + reset + " or " + blue + "NO" + reset), "normal");
            String userInputString = userInput.nextLine();

            if (userInputString.toLowerCase().equals("yes"))
            {
                this.viewGameFight();
                validInput = true;
                this.viewGameMovement();
                return;
            }
            else if (userInputString.toLowerCase().equals("no"))
            {
                this.viewGameEncounter();
                validInput = true;
                return;
            }
            else if (userInputString.toLowerCase().contains("close"))
            {
                Main.gameController.controllerCloseGame("close");
                return;
            }
            this.viewMessage("Invalid Input", "error");
        }
    }

    private void viewGameFight() throws SQLException, IOException, BadLocationException, InterruptedException {
        GameController gameController = Main.gameController;

        boolean someoneDies = false;
        Enemy enemy = gameController.controllerFetchEnemy();

        while (!someoneDies)
        {
            this.viewTopBanner();
            this.viewEnemyBanner(enemy);

            someoneDies = gameController.controllerCombat(enemy);
            if (someoneDies)
                return;
        }
    }

    @Override
    public void viewEndGame()
    {
        //http://patorjk.com/software/taag/#p=display&f=Larry%203D&t=game%20over
        this.viewMessage(red + "   __      __      ___ ___      __         ___   __  __     __   _ __   " + reset, "error");
        this.viewMessage(red + " /'_ `\\  /'__`\\  /' __` __`\\  /'__`\\      / __`\\/\\ \\/\\ \\  /'__`\\/\\`'__\\ " + reset, "error");
        this.viewMessage(red + "/\\ \\L\\ \\/\\ \\L\\.\\_/\\ \\/\\ \\/\\ \\/\\  __/     /\\ \\L\\ \\ \\ \\_/ |/\\  __/\\ \\ \\/  " + reset, "error");
        this.viewMessage(red + "\\ \\____ \\ \\__/.\\_\\ \\_\\ \\_\\ \\_\\ \\____\\    \\ \\____/\\ \\___/ \\ \\____\\\\ \\_\\" + reset, "error");
        this.viewMessage(red + " \\/___L\\ \\/__/\\/_/\\/_/\\/_/\\/_/\\/____/     \\/___/  \\/__/   \\/____/ \\/_/ " + reset, "error");
        this.viewMessage(red + "   /\\____/  " + reset, "error");
        this.viewMessage(red + "   \\_/__/ " + reset, "error");
    }

    @Override
    public void viewWinGame()
    {

        //http://patorjk.com/software/taag/#p=display&f=Larry%203D&t=you%20win%0A
        this.viewMessage(blue + "                                        __" + reset, "normal");
        this.viewMessage(blue + " __  __    ___   __  __      __  __  __/\\_\\    ___" + reset, "normal");
        this.viewMessage(blue + "/\\ \\/\\ \\  / __`\\/\\ \\/\\ \\    /\\ \\/\\ \\/\\ \\/\\ \\ /' _ `\\" + reset, "normal");
        this.viewMessage(blue + "\\ \\ \\_\\ \\/\\ \\L\\ \\ \\ \\_\\ \\   \\ \\ \\_/ \\_/ \\ \\ \\/\\ \\/\\ \\" + reset, "normal");
        this.viewMessage(blue + " \\/`____ \\ \\____/\\ \\____/    \\ \\___x___/'\\ \\_\\ \\_\\ \\_\\" + reset, "normal");
        this.viewMessage(blue + "  `/___/> \\/___/  \\/___/      \\/__//__/   \\/_/\\/_/\\/_/" + reset, "normal");
        this.viewMessage(blue + "     /\\___/" + reset, "normal");
        this.viewMessage(blue + "     \\/__/" + reset, "normal");
    }

    @Override
    public void viewMessage(String message, String type)
    {
        // Convert colours accordingly
        String replaceString = message;

        replaceString = replaceString.replace("co:blue", blue);
        replaceString = replaceString.replace("co:reset", reset);
        replaceString = replaceString.replace("co:grey", grey);
        replaceString = replaceString.replace("co:green", green);
        replaceString = replaceString.replace("co:purple", purple);
        replaceString = replaceString.replace("co:red", red);

        System.out.println(type == "error" ? ("[" + red + "ERROR" + reset + "] " + replaceString) : ("[" + blue + "SWINGY" + reset + "] " + replaceString));
    }

    @Override
    public void viewMap() throws IOException, BadLocationException {
        GameModel gameModel = Main.gameController.controllerReturnModel();
        Map gameMap = gameModel.gameMap;
        Hero player = gameModel.player;

        String table = "\n";
        String member;
        int mapSize = gameMap.mapSize;

        for (int y = 1; y <= mapSize; y++)
        {
            for (int x = 1; x <= mapSize; x++)
            {
                member = "[";
                String entity = "";
                if (player.getPosition().getCoordinateX() == x && player.getPosition().getCoordinateY() == y)
                    entity += (blue + "*" + reset);
                for (int i = 0; i < gameModel.enemies.size(); i++)
                {
                    Position enemPos = gameModel.enemies.get(i).getPosition();
                    if (enemPos.getCoordinateX() == x && enemPos.getCoordinateY() == y)
                        entity += (red + "*" + reset);
                    if (player.getPosition().getCoordinateX() == x && player.getPosition().getCoordinateY() == y &&
                            enemPos.getCoordinateX() == x && enemPos.getCoordinateY() == y)
                        entity = (purple+ "*" + reset);
                }
                if (entity.length() == 0)
                    entity = " ";
                member += entity;
                member += "]";
                table += member;
            }
            table += "\n";
        }

        this.viewMessage(table, "normal");
        //gameModel.enemies
    }

    @Override
    public void viewTopBanner()
    {
        Hero hero = gameController.controllerReturnModel().player;
        int heroLevel = gameController.controllerReturnModel().workoutLevel(hero.getExperience());

        this.viewMessage(("--------------------------------[ " + blue + hero.getName().toUpperCase() + blue + " |" + grey + " Level: " + reset + heroLevel + blue + " |" + grey + " XP: " + reset + hero.getExperience() + blue + " | " + grey + "HP: " + reset + hero.getHitPoints()+ " ]---------------------------------"), "normal");
    }

    @Override
    public void viewBottomBanner()
    {
        this.viewMessage("------------------------------------------------------------------------------------------------------------------", "normal");

    }

    @Override
    public void viewEnemyBanner(Enemy enemy)
    {
        this.viewMessage("----------------------------------------------[ " + red + "ENEMY" + " | " + grey + "HP: " +reset + enemy.getHitPoints() +" ]----------------------------------------------", "normal");
        return;
    }

    @Override
    public boolean viewFormatSaves(List<Hero> saves)
    {
        if (saves.isEmpty())
            return false;

        this.viewMessage("-----------------------------------------------------[ " + blue  + "Saves" + reset + " ]----------------------------------------------------", "normal");

        for (int i = 0; i < saves.size(); i++)
        {
            Hero hero = saves.get(i);

            int heroLevel = gameController.controllerReturnModel().workoutLevel(hero.getExperience());

            // System.out.println("characterName : " +  hero.getName());
            // System.out.println("characterPosX : " +  hero.getPosition().getCoordinateX());
            // System.out.println("characterPosY : " +  hero.getPosition().getCoordinateY());
            // System.out.println("characterXP : " +  hero.getExperience());
            // System.out.println("characterHP : " +  hero.getHitPoints());
            // System.out.println("characterAP: " +  hero.getAttackPoints());
            // System.out.println("characterDP: " +  hero.getDefencePoints());

            String savedHero = "    " + blue + hero.getName().toUpperCase() + blue + "      |" + grey + " Level: " + reset + heroLevel + blue + " |" + grey + " XP: " + reset + hero.getExperience() + blue + " | " + grey + "HP: " + reset + hero.getHitPoints() + "   ";
            this.viewMessage(savedHero, "normal");

        }
        this.viewMessage("------------------------------------------------------------------------------------------------------------------", "normal");
        return true;
    }

    @Override
    public void onClose()
    {
        return;
    }
}


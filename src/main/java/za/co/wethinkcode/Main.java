package za.co.wethinkcode;

import za.co.wethinkcode.controller.GameController;
import za.co.wethinkcode.model.GameModel;
import za.co.wethinkcode.view.ConsoleView;
import za.co.wethinkcode.view.GUIView;
import za.co.wethinkcode.view.GameView;

import java.io.IOException;
import java.sql.SQLException;

import lombok.Getter;
import lombok.Setter;

import javax.swing.text.BadLocationException;

@Getter
@Setter
public class Main
{
    public static GameModel gameModel           = null;
    public static GameController gameController = null;
    public static GameView gameView             = null;

    public static void main(String[] args)
    {
        try
        {
            // Runtime.getRuntime().exec("/usr/bin/open -a Terminal");



            gameModel               = new GameModel();
            gameController          = new GameController();

            ConsoleView tempview    = new ConsoleView();
            gameView                = tempview.viewInitialiseGame(args);

            gameView.viewSelectGame();

        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (BadLocationException e)
        {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return;
    }
}


 /*
     mvn clean package
     java -cp target/swing-1.0-SNAPSHOT.jar za.co.wethinkcode.Main

    mvn clean compile assembly:single package
    java -cp target/swing-1.0-SNAPSHOT-jar-with-dependencies.jar za.co.wethinkcode.Main
 */
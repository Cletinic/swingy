package za.co.wethinkcode.controller;

import za.co.wethinkcode.Main;
import za.co.wethinkcode.model.GameModel;
import za.co.wethinkcode.model.Map;
import za.co.wethinkcode.model.Position;
import za.co.wethinkcode.model.character.enemies.Enemy;
import za.co.wethinkcode.model.character.heros.Hero;
import za.co.wethinkcode.model.character.heros.Rogue;
import za.co.wethinkcode.view.ConsoleView;
import za.co.wethinkcode.view.GUIView;
import za.co.wethinkcode.view.GameView;

import javax.swing.text.BadLocationException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class GameController
{
    // VIEW > MODEL
    public GameModel controllerReturnModel()
    {
        return Main.gameModel;
    }

    public List<Hero> controllerGetSaves() throws SQLException
    {
        return Main.gameModel.gameLogger.showAllFromDataBase();
    }

    public boolean controllerFetchSave(String name) throws SQLException, IOException, BadLocationException, InterruptedException {
        GameModel gameModel = Main.gameModel;
        Hero fetchedHero    = gameModel.gameLogger.fetchFromDataBase(name);

        if (fetchedHero != null)
        {
            gameModel.player    = fetchedHero;

            gameModel.gameMap   = new Map();
            gameModel.setEnemyPositions(gameModel.gameMap.mapSize);

            Main.gameView.viewGameMovement();

            return true;
        }

        return false;
    }

    public boolean controllerSetNewGame(String name) throws SQLException, IOException, BadLocationException, InterruptedException {
        GameModel gameModel = Main.gameModel;

        Hero fetchedHero    = gameModel.gameLogger.fetchFromDataBase(name);

        if (fetchedHero == null)
        {
            gameModel.player    = new Rogue(name);
            gameModel.gameMap   = new Map();
            gameModel.setEnemyPositions(gameModel.gameMap.mapSize);

            gameModel.gameLogger.saveToDataBase(gameModel.player);

            Main.gameView.viewGameMovement();

            return true;
        }

        return false;
    }

    public int controllerMovement(String direction) throws SQLException, IOException, BadLocationException, InterruptedException {
        GameModel gameModel = Main.gameModel;

        gameModel.player.characterMovement(direction);
        return gameModel.gameMap.detectCollision();
    }

    public boolean controllerAttemptRun()
    {
        GameModel gameModel = Main.gameModel;

        if (gameModel.randomGen(1, 100) % 2 == 0)
        {
            gameModel.player.setPosition(gameModel.player.getPreviousPosition());
            gameModel.player.gainLife(5);
            return true;
        }

        return false;
    }

    public Enemy controllerFetchEnemy()
    {
        return Main.gameModel.fetchEnemy();
    }

    public boolean controllerCombat(Enemy enemy) throws SQLException, IOException, BadLocationException, InterruptedException {
        return  Main.gameModel.combat(enemy);
    }

    public void controllerCloseGame(String closeType) throws SQLException, IOException, BadLocationException, InterruptedException {
        Main.gameModel.endGame(closeType);
        return;
    }

    public void controllerSwitchView(String currentFunction)
    {
        try
        {

            String type = Main.gameView.getClass().getName();
            Main.gameView.onClose();
            if (type.toLowerCase().contains("guiview"))
                Main.gameView = new ConsoleView();
            else if (type.toLowerCase().contains("consoleview"))
                Main.gameView = new GUIView();
            else
                return;

            switch(currentFunction.toLowerCase())
            {
                case "viewselectgame":
                    Main.gameView.viewSelectGame();
                    break;
                case "viewnewgame":
                    Main.gameView.viewNewGame();
                    break;
                case "viewloadgame":
                    Main.gameView.viewLoadGame();
                    break;
                case "viewgameencounter":
                    Main.gameView.viewGameEncounter();
                    break;
                case "viewgamemovement":
                    Main.gameView.viewGameMovement();
                    break;
                case "viewendgame":
                    Main.gameView.viewEndGame();
                    break;
                case "viewwingame":
                    Main.gameView.viewWinGame();
                    break;
                default:
                    Main.gameView.viewGameMovement();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (BadLocationException e)
        {
            e.printStackTrace();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        return;
    }

    // VIEW < MODEL
    public void controllerStringMedium(String message, String type) throws IOException, BadLocationException
    {
        Main.gameView.viewMessage(message, type);
    }

    public void controllerGameState(String state) throws IOException, BadLocationException {
        GameView gameView = Main.gameView;

        switch (state.toLowerCase())
        {
            case "win":
            {
                gameView.viewWinGame();
                return;
            }
            case "dead":
            {
                gameView.viewEndGame();
                return;
            }
            case "close":
            {
                gameView.viewMessage("Closing game...", "normal");
                break;
            }
            default:
            {
                break;
            }
        }
    }
}
